\contentsline {paragraph}{}{2}% 
\contentsline {paragraph}{}{3}% 
\contentsline {chapter}{\numberline {1}Introduction}{6}% 
\contentsline {paragraph}{}{6}% 
\contentsline {paragraph}{}{6}% 
\contentsline {paragraph}{}{6}% 
\contentsline {paragraph}{}{7}% 
\contentsline {chapter}{\numberline {2}Literature Review}{8}% 
\contentsline {section}{\numberline {2.1}Current Literature}{8}% 
\contentsline {paragraph}{}{8}% 
\contentsline {paragraph}{}{8}% 
\contentsline {paragraph}{}{9}% 
\contentsline {paragraph}{}{9}% 
\contentsline {section}{\numberline {2.2}Further Research Opportunities}{10}% 
\contentsline {paragraph}{}{10}% 
\contentsline {paragraph}{}{11}% 
\contentsline {subsection}{\numberline {2.2.1}Detecting other types of depressive symptoms}{11}% 
\contentsline {paragraph}{}{11}% 
\contentsline {paragraph}{}{11}% 
\contentsline {subsubsection}{Mental Symptoms}{12}% 
\contentsline {paragraph}{Sustained and marked diminished interest (apathy)}{12}% 
\contentsline {subparagraph}{}{12}% 
\contentsline {subparagraph}{}{12}% 
\contentsline {paragraph}{Suicidal Ideation}{12}% 
\contentsline {subparagraph}{}{12}% 
\contentsline {subsubsection}{Physical symptoms}{13}% 
\contentsline {paragraph}{Fatigue or loss of energy nearly every day}{13}% 
\contentsline {subparagraph}{}{13}% 
\contentsline {paragraph}{Weight loss/gain when not dieting}{13}% 
\contentsline {subparagraph}{}{13}% 
\contentsline {subsection}{\numberline {2.2.2}Detecting other mental illnesses}{13}% 
\contentsline {subsubsection}{Anxiety Disorders}{13}% 
\contentsline {paragraph}{}{13}% 
\contentsline {paragraph}{}{13}% 
\contentsline {paragraph}{}{14}% 
\contentsline {subsubsection}{Other mental disorders}{14}% 
\contentsline {paragraph}{}{14}% 
\contentsline {chapter}{\numberline {3}Data Collection}{15}% 
\contentsline {paragraph}{}{15}% 
\contentsline {section}{\numberline {3.1}Reddit}{15}% 
\contentsline {paragraph}{}{15}% 
\contentsline {paragraph}{}{15}% 
\contentsline {subsection}{\numberline {3.1.1}The r/Anxiety Subreddit}{16}% 
\contentsline {paragraph}{}{16}% 
\contentsline {paragraph}{}{17}% 
\contentsline {paragraph}{}{17}% 
\contentsline {subsection}{\numberline {3.1.2}The r/depression Subreddit}{18}% 
\contentsline {paragraph}{}{18}% 
\contentsline {subsection}{\numberline {3.1.3}Collecting Data from Reddit}{18}% 
\contentsline {subsubsection}{Reddit API}{18}% 
\contentsline {paragraph}{}{18}% 
\contentsline {subsubsection}{PRAW}{19}% 
\contentsline {paragraph}{}{19}% 
\contentsline {paragraph}{}{20}% 
\contentsline {subsubsection}{The Pushshift API}{21}% 
\contentsline {paragraph}{}{21}% 
\contentsline {paragraph}{}{22}% 
\contentsline {paragraph}{}{22}% 
\contentsline {section}{\numberline {3.2}Building Datasets}{22}% 
\contentsline {subsection}{\numberline {3.2.1}Dataset Format}{22}% 
\contentsline {paragraph}{}{22}% 
\contentsline {paragraph}{}{23}% 
\contentsline {paragraph}{}{24}% 
\contentsline {subsection}{\numberline {3.2.2}The Anxiety and Depression Reddit Datasets}{24}% 
\contentsline {subsubsection}{Data Collection}{24}% 
\contentsline {paragraph}{}{24}% 
\contentsline {paragraph}{}{24}% 
\contentsline {paragraph}{}{25}% 
\contentsline {subsubsection}{Dataset Cleaning}{25}% 
\contentsline {paragraph}{}{25}% 
\contentsline {paragraph}{Step 1: Removing drug-related submissions}{25}% 
\contentsline {subparagraph}{}{25}% 
\contentsline {subparagraph}{}{26}% 
\contentsline {paragraph}{Step 2: Splitting paragraphs within submissions}{26}% 
\contentsline {subparagraph}{}{26}% 
\contentsline {subparagraph}{}{26}% 
\contentsline {paragraph}{Step 3: Dealing with URLs inside the text content}{27}% 
\contentsline {subparagraph}{}{27}% 
\contentsline {subparagraph}{}{27}% 
\contentsline {paragraph}{Step 4: Dealing with other issues}{27}% 
\contentsline {subparagraph}{}{27}% 
\contentsline {subsection}{\numberline {3.2.3}The Control Reddit Dataset}{28}% 
\contentsline {paragraph}{}{28}% 
\contentsline {paragraph}{}{28}% 
\contentsline {paragraph}{}{28}% 
\contentsline {paragraph}{}{29}% 
\contentsline {paragraph}{}{29}% 
\contentsline {subsection}{\numberline {3.2.4}Ethical Considerations}{30}% 
\contentsline {paragraph}{}{30}% 
\contentsline {chapter}{\numberline {4}Analysis}{31}% 
\contentsline {paragraph}{}{31}% 
\contentsline {section}{\numberline {4.1}Text Preprocessing}{31}% 
\contentsline {paragraph}{}{32}% 
\contentsline {paragraph}{}{32}% 
\contentsline {paragraph}{}{32}% 
\contentsline {paragraph}{}{32}% 
\contentsline {paragraph}{}{33}% 
\contentsline {section}{\numberline {4.2}Exploratory Analysis}{33}% 
\contentsline {subsection}{\numberline {4.2.1}Dataset Description}{33}% 
\contentsline {paragraph}{}{33}% 
\contentsline {paragraph}{}{34}% 
\contentsline {paragraph}{}{34}% 
\contentsline {subsection}{\numberline {4.2.2}Semantic Analysis}{35}% 
\contentsline {paragraph}{}{35}% 
\contentsline {subsubsection}{Individual Word Frequency Analysis}{35}% 
\contentsline {paragraph}{}{35}% 
\contentsline {paragraph}{}{35}% 
\contentsline {paragraph}{}{36}% 
\contentsline {paragraph}{}{37}% 
\contentsline {paragraph}{}{37}% 
\contentsline {paragraph}{}{38}% 
\contentsline {paragraph}{}{39}% 
\contentsline {paragraph}{}{39}% 
\contentsline {subsubsection}{N-Gram Analysis}{40}% 
\contentsline {paragraph}{}{40}% 
\contentsline {paragraph}{}{40}% 
\contentsline {paragraph}{}{40}% 
\contentsline {paragraph}{}{42}% 
\contentsline {subsection}{\numberline {4.2.3}Sentiment Analysis}{43}% 
\contentsline {paragraph}{}{43}% 
\contentsline {subsubsection}{Sentiment Analysis Models}{44}% 
\contentsline {paragraph}{VADER}{44}% 
\contentsline {subparagraph}{}{44}% 
\contentsline {paragraph}{Flair}{44}% 
\contentsline {subparagraph}{}{44}% 
\contentsline {subsubsection}{Sentiment Score Distribution}{44}% 
\contentsline {paragraph}{}{44}% 
\contentsline {paragraph}{}{44}% 
\contentsline {paragraph}{}{45}% 
\contentsline {section}{\numberline {4.3}Text Classification Models}{47}% 
\contentsline {paragraph}{}{47}% 
\contentsline {paragraph}{}{47}% 
\contentsline {paragraph}{Data Loading}{48}% 
\contentsline {subparagraph}{}{48}% 
\contentsline {subsection}{\numberline {4.3.1}Chosen Performance Metrics}{48}% 
\contentsline {paragraph}{}{48}% 
\contentsline {subsubsection}{Confusion Matrix}{48}% 
\contentsline {paragraph}{}{48}% 
\contentsline {paragraph}{}{48}% 
\contentsline {subsubsection}{Accuracy, Recall and $F_1$ score}{49}% 
\contentsline {paragraph}{}{49}% 
\contentsline {paragraph}{}{49}% 
\contentsline {paragraph}{}{50}% 
\contentsline {paragraph}{}{50}% 
\contentsline {subsection}{\numberline {4.3.2}Sentiment-Only Models}{50}% 
\contentsline {paragraph}{}{50}% 
\contentsline {paragraph}{}{50}% 
\contentsline {paragraph}{}{51}% 
\contentsline {paragraph}{}{52}% 
\contentsline {subsection}{\numberline {4.3.3}Vectoriser-Classifier Models}{54}% 
\contentsline {paragraph}{}{54}% 
\contentsline {subsubsection}{Count Vectorizer with Support Vector Machine}{54}% 
\contentsline {paragraph}{}{54}% 
\contentsline {paragraph}{}{54}% 
\contentsline {paragraph}{}{55}% 
\contentsline {paragraph}{}{55}% 
\contentsline {paragraph}{}{56}% 
\contentsline {paragraph}{}{56}% 
\contentsline {paragraph}{}{57}% 
\contentsline {subsubsection}{Tfidf Vectorizer with Support Vector Machine}{58}% 
\contentsline {paragraph}{}{58}% 
\contentsline {paragraph}{}{58}% 
\contentsline {paragraph}{}{59}% 
\contentsline {paragraph}{}{59}% 
\contentsline {subsubsection}{Hyperparameter Tuning and Grid Search}{61}% 
\contentsline {paragraph}{}{61}% 
\contentsline {paragraph}{}{61}% 
\contentsline {paragraph}{}{63}% 
\contentsline {subsubsection}{Hierarchical Models}{63}% 
\contentsline {paragraph}{}{63}% 
\contentsline {paragraph}{}{64}% 
\contentsline {paragraph}{}{65}% 
\contentsline {subsection}{\numberline {4.3.4}Word Embeddings Models}{65}% 
\contentsline {paragraph}{}{65}% 
\contentsline {paragraph}{}{66}% 
\contentsline {paragraph}{}{66}% 
\contentsline {paragraph}{}{68}% 
\contentsline {paragraph}{}{68}% 
\contentsline {paragraph}{}{68}% 
\contentsline {subsection}{\numberline {4.3.5}Deep Learning Models}{69}% 
\contentsline {paragraph}{}{69}% 
\contentsline {subsubsection}{LSTM Models}{69}% 
\contentsline {paragraph}{}{69}% 
\contentsline {subsubsection}{Text Preprocessing}{70}% 
\contentsline {paragraph}{}{70}% 
\contentsline {paragraph}{}{70}% 
\contentsline {paragraph}{}{70}% 
\contentsline {subsubsection}{Network Architecture}{71}% 
\contentsline {paragraph}{}{71}% 
\contentsline {paragraph}{}{71}% 
\contentsline {subsubsection}{Training and Final Results}{72}% 
\contentsline {paragraph}{}{72}% 
\contentsline {paragraph}{}{73}% 
\contentsline {paragraph}{}{74}% 
\contentsline {paragraph}{}{75}% 
\contentsline {chapter}{\numberline {5}Conclusions}{77}% 
\contentsline {section}{\numberline {5.1}Results Summary and Discussion}{77}% 
\contentsline {paragraph}{}{77}% 
\contentsline {paragraph}{}{78}% 
\contentsline {section}{\numberline {5.2}Areas of Future Development}{78}% 
\contentsline {paragraph}{More Sophisticated Deep Learning Models}{78}% 
\contentsline {subparagraph}{}{78}% 
\contentsline {paragraph}{Generalisation to Other Social Media}{79}% 
\contentsline {subparagraph}{}{79}% 
\contentsline {paragraph}{Model Application for Mood Detection}{79}% 
\contentsline {subparagraph}{}{79}% 
\contentsline {paragraph}{}{79}% 
