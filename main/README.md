This repository contains the code and data used in the context of the study:
"Can NLP help detect signs of depression and anxiety in social media?".
This project is led by Hedi Chaibi, an MSc Computing Science student
at Imperial College London, under the supervision of Dr Anandha Gopalan.
The data collected for the purpose of this study is not open to public use.

The directories in the "main" folder are:

* data: contains the raw and preprocessed data. Also contains some data utils files
used un the construction of datasets

* keys: contains .pem and .ppk keys to connect to AWS EC2 instances. Also contains a
zip file that consists of all files sent over to the instance over ssh in order to
be able to run the Python scrips

* models: contains the pickle dumps of several models

* src: contains the source files of the project

    * data: contains the source code related to constructing datasets
    * models: contains the source code related to training and testing models
    * utils: contains the utils.py file that groups all coded utils functions
    * visualition: mostly contains source code related to exploratory analysis
