import json
import random
import time

import pandas as pd
import praw
from prawcore import exceptions


def populate(number_of_posts, number_of_subreddits, number_of_comments, dataset, subreddit_list, subreddits_used,
             lower_limit_text=50,
             upper_limit_text=500):
    with open(dataset, encoding="utf-8") as f:
        json_file = json.load(f)

    original_length = len(json_file)

    print(f"Loading dataset... length = {original_length}.")

    list_subreddits_used = pd.read_csv(subreddits_used, header=None).squeeze().tolist()

    dicos = []

    subreddit_list = pd.read_csv(subreddit_list)["subreddit"].tolist()

    r = praw.Reddit(client_id="zaIF2bRYQMUXaA", client_secret="mTvv49qCkbOTGDtJ-0boCkku4yI",
                    username="Ibiahc", password="Adsl2016@T//", user_agent="RedditAPIv1")

    for i in range(number_of_subreddits):

        random_subreddit = random.choice(subreddit_list)

        while random_subreddit in list_subreddits_used:
            random_subreddit = random.choice(subreddit_list)

        list_subreddits_used.append(random_subreddit)

        print(f"\nThe random subreddit is {random_subreddit} ({i}/{number_of_subreddits})\n")

        sub = r.subreddit(random_subreddit)
        posts = sub.hot(limit=number_of_posts)

        counter_post = 0

        try:

            for post in posts:

                counter_post += 1
                print(f"Doing post number {counter_post}/{number_of_posts}")

                comments = post.comments
                counter = number_of_comments

                for comment in comments:

                    try:

                        dico = {"subreddit": random_subreddit, "post_id": post.id,
                                "comment_id": comment.id, "selftext": comment.body, "length": len(comment.body),
                                "score": comment.score,
                                "stickied": comment.stickied}

                    except AttributeError:

                        print("There was an attribute error on the comment (possibly MoreComments object). Breaking.")

                        break

                    if lower_limit_text <= dico.get("length") <= upper_limit_text:
                        dicos.append(dico)

                    counter -= 1

                    if counter == 0:
                        break

            time.sleep(2)

        except exceptions.NotFound:
            print("NotFound Exception raised. Continuing.")
            continue

        except exceptions.Forbidden:
            print("Forbidden Exception raised. Continuing")
            continue

    json_file.extend(dicos)

    print(
        f"Now adding {len(dicos)} instances to the file, bringing the total from {original_length} to {len(json_file)}")

    with open(dataset, "w", encoding="utf-8") as f:
        json.dump(json_file, f, ensure_ascii=False, indent=4)

    pd.Series(list_subreddits_used).to_csv(subreddits_used, header=False, index=False)


def main():
    populate(50, 50, 8, "control_dataset_raw.json", "subreddit_listv2.csv", "subreddits_used.csv", upper_limit_text=750)


if __name__ == "__main__":
    main()
