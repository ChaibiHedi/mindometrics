import json
import re

import numpy as np
import pandas as pd
from cryptography.fernet import Fernet

from src.utils.utils import load_json, dump_json


def delete_id_from_jsonfile(input_file, output_file, list_of_ids, identifier="id"):
    json_file = load_json(input_file)

    new_json_file = [x for x in json_file if x[identifier] not in list_of_ids]

    with open(output_file, "w", encoding='utf-8') as f:
        json.dump(new_json_file, f, ensure_ascii=False, indent=4)

    number_files_removed = len(json_file) - len(new_json_file)
    print(
        f"There were {number_files_removed} instances removed from the dataset, i.e. {round(number_files_removed * 100 / len(json_file))}%.")


def split_text_json(input_file, output_file, splitted_field, identifier="id", sep="\n\n"):
    json_file = load_json(input_file)

    new_dictionaries = []

    for dico in json_file:

        splitted_text = dico[splitted_field].split(sep)
        counter = 0

        for text in splitted_text:
            dico[splitted_field] = text
            new_dico = dict(dico)
            new_dico[identifier] = dico[identifier] + "/" + str(counter)
            new_dictionaries.append(new_dico)
            counter += 1

    dump_json(output_file, new_dictionaries)

    return new_dictionaries


def delete_medication_posts(dataset, words_list, identifier="id"):
    unwanted_ids = []
    drugs_list = list(map(str.lower, words_list))

    json_file = load_json(dataset)

    for sub in json_file:
        original_text = sub["selftext"]
        bag_of_words = generate_bow(original_text)
        check = any(item in bag_of_words for item in drugs_list)
        if check:
            unwanted_ids.append(sub[identifier])

    return unwanted_ids


def generate_bow(original_text):
    bag_of_words = list(map(str.lower, re.findall(r"[\w']+", original_text)))
    return bag_of_words


def delete_text_abnormalities(dataset, identifier="id"):
    unwanted_ids = []

    json_file = load_json(dataset)

    for sub in json_file:

        if sub["selftext"] == "[removed]" or sub["selftext"] == "[deleted]" or sub["selftext"] == "":
            unwanted_ids.append(sub[identifier])

    return unwanted_ids


def delete_vshort_posts(dataset, threshold=15, identifier="id"):
    unwanted_ids = []

    json_file = load_json(dataset)

    for sub in json_file:

        if len(sub["selftext"]) < threshold:
            unwanted_ids.append(sub[identifier])

    return unwanted_ids


def delete_long_messages(dataset, percentile=0.975):
    json_file = load_json(dataset)
    body_texts = []

    for sub in json_file:
        body_texts.append(sub["selftext"])

    body_lengths = [len(x) for x in body_texts]

    threshold = np.percentile(body_lengths, percentile * 100)

    unwanted_ids = [sub["id"] for sub in json_file if len(sub["selftext"]) > threshold]

    return unwanted_ids


def delete_notabene_posts(dataset, identifier="id"):
    unwanted_ids = []
    json_file = load_json(dataset)

    for sub in json_file:

        bagofwords = generate_bow(sub["selftext"])

        if "edit" in bagofwords:
            unwanted_ids.append(sub[identifier])

    return unwanted_ids


def delete_http_posts(dataset, identifier="id"):
    unwanted_ids = []
    json_file = load_json(dataset)

    for sub in json_file:

        if "http" in sub["selftext"]:
            unwanted_ids.append(sub[identifier])

    return unwanted_ids


def delete_bot_comments(dataset):
    unwanted_ids = []
    json_file = load_json(dataset)

    for sub in json_file:

        if "I am a bot" in sub["selftext"]:
            unwanted_ids.append(sub["comment_id"])

    return unwanted_ids


def replace_special_characters(dataset, character):
    json_file = load_json(dataset)

    for sub in json_file:

        if character in sub["selftext"]:
            sub["selftext"] = sub["selftext"].replace(character, "")

    dump_json(dataset, json_file)

    return True


def encrypt_author_names(dataset):
    json_file = load_json(dataset)

    for sub in json_file:
        author_name = sub["author"]
        author_encoded = author_name.encode()
        key = open("secret.key", "rb").read()
        f = Fernet(key)
        encrypted_author = f.encrypt(author_encoded)

        sub["author"] = str(encrypted_author)

    dump_json(dataset, json_file)


def replace_http_posts(output_file):
    json_file = load_json(output_file)

    for sub in json_file:

        if "http" in sub["selftext"]:
            sub["selftext"] = re.sub('https?://[A-Za-z0-9./]+', '', sub["selftext"])

    dump_json(output_file, json_file)


def clean_up(raw_dataset, output_file):
    drugs_list = pd.read_csv("../../data/data_utils/depression_medicationlist.csv").squeeze().tolist()

    unwanted_ids = delete_text_abnormalities(raw_dataset)
    unwanted_ids.extend(delete_medication_posts(raw_dataset, drugs_list))
    delete_id_from_jsonfile(raw_dataset, output_file, unwanted_ids)

    split_text_json(output_file, output_file, "selftext")

    replace_http_posts(output_file)

    unwanted_ids = delete_vshort_posts(output_file, threshold=30)
    unwanted_ids.extend(delete_notabene_posts(output_file))
    unwanted_ids.extend(delete_long_messages(output_file))
    delete_id_from_jsonfile(output_file, output_file, unwanted_ids)

    replace_special_characters(output_file, "\n")


def clean_up_control(raw_dataset, output_file):
    unwanted_ids = delete_http_posts(raw_dataset, "comment_id")
    unwanted_ids.extend(delete_bot_comments(raw_dataset))
    delete_id_from_jsonfile(raw_dataset, output_file, unwanted_ids, "comment_id")

    split_text_json(output_file, output_file, "selftext", identifier="comment_id")

    unwanted_ids = delete_vshort_posts(output_file, threshold=30, identifier="comment_id")
    unwanted_ids.extend(delete_notabene_posts(output_file, "comment_id"))

    delete_id_from_jsonfile(output_file, output_file, unwanted_ids, "comment_id")

    replace_special_characters(output_file, "\n")


def main():
    clean_up("../../data/raw/anxiety_dataset_raw.json", "../../data/processed/anxiety_dataset_clean.json")
    encrypt_author_names("../../data/processed/anxiety_dataset_clean.json")

    clean_up("../../data/raw/depression_dataset_raw.json", "../../data/processed/depression_dataset_clean.json")
    encrypt_author_names("../../data/processed/depression_dataset_clean.json")


if __name__ == "__main__":
    main()
