import time

import pandas as pd
import praw
from prawcore import exceptions

from src.utils.utils import add_instances_to_json_file, get_field_list_from_json


class DatabasePopulater:

    def __init__(self, subreddit_name, unwanted_flairs=[]):

        self.reddit = None
        self.subreddit_name = subreddit_name
        self.is_connected = False
        self.unwanted_flairs = unwanted_flairs
        self.fields = ('id', 'title', 'selftext', 'score', 'link_flair_text')

    def connect(self, client_id, client_secret, username, password, user_agent):

        self.reddit = praw.Reddit(client_id=client_id,
                                  client_secret=client_secret,
                                  username=username,
                                  password=password,
                                  user_agent=user_agent)

        self.reddit.config.store_json_result = True

        self.is_connected = True

    def download_top(self, limit=1000):

        if self.is_connected:

            subreddit = self.reddit.subreddit(self.subreddit_name)

            top_submissions = subreddit.top(time_filter="year", limit=limit)

            query_results = []

            ids_so_far = []

            unwanted_flairs = self.unwanted_flairs

            for submission in top_submissions:

                if not submission.stickied and submission.link_flair_text not in unwanted_flairs:
                    sub_dict = self.__extract_dict(submission)
                    query_results.append(sub_dict)

                    ids_so_far.append(sub_dict["id"])

            return query_results

    def populate(self, number_posts, list_of_ids, tested_ids, output_json_path):

        if self.is_connected:

            ids_to_test = list(set(list_of_ids) - set(tested_ids))

            print(f"There are {len(ids_to_test)} original ids to test in the list provided.\n")

            if len(ids_to_test) == 0:
                return []

            counter = 0
            query_results = []
            used_ids = []

            for sub_id in ids_to_test:

                if number_posts < len(ids_to_test):
                    display_counter = number_posts

                else:
                    display_counter = len(ids_to_test)

                used_ids.append(sub_id)
                counter += 1

                if sub_id in tested_ids:

                    print(f"Post with id: {sub_id} is already in database.")

                else:

                    try:

                        submission = self.reddit.submission(id=sub_id)

                        if not submission.stickied and submission.link_flair_text not in self.unwanted_flairs \
                                and submission.selftext != "[deleted]" and submission.selftext != "" \
                                and submission.selftext != "[removed]":

                            print(
                                f"({counter}/{display_counter}) Adding submission with title <{submission.title}> to results.")

                            sub_dict = self.__extract_dict(submission)
                            query_results.append(sub_dict)

                        else:

                            print(
                                f"({counter}/{display_counter}) Not adding submission with title <{submission.title}> because it does not fulfill "
                                f"selection criteria")

                    except exceptions.NotFound:

                        print(f"Exception raised for submission {sub_id}, moving on to next submission.")
                        continue

                if counter == number_posts:
                    break

                time.sleep(0.5)

            add_instances_to_json_file(query_results, output_json_path)
            print(f"\nAdded {len(query_results)} instances to json file.")

            distinct_ids = len(set(get_field_list_from_json("id", output_json_path)))

            print(f"There are now {distinct_ids} distinct ids in the json file.")

            return used_ids

        else:

            print("Please establish connection with Reddit API.")

    def __extract_dict(self, submission):

        to_dict = vars(submission)
        sub_dict = {field: to_dict[field] for field in self.fields}
        sub_dict["author"] = str(submission.author)
        return sub_dict


def main():
    populate(500, "depression", "ids_new_method2.csv", "depression_tested_ids.csv", "depression_dataset_raw.json")


def populate(num_posts, subreddit_name, path_to_sub_ids, path_to_tested_ids,
             output_json, unwanted_flairs=[]):
    populater = DatabasePopulater(subreddit_name, unwanted_flairs=unwanted_flairs)

    populater.connect(client_id="zaIF2bRYQMUXaA", client_secret="mTvv49qCkbOTGDtJ-0boCkku4yI",
                      username="Ibiahc", password="Adsl2016@T//", user_agent="RedditAPIv1")

    sub_ids = pd.read_csv(path_to_sub_ids).squeeze().tolist()
    tested_ids = pd.read_csv(path_to_tested_ids).squeeze().tolist()

    new_tested_ids = populater.populate(number_posts=num_posts, list_of_ids=sub_ids, tested_ids=tested_ids,
                                        output_json_path=output_json)

    tested_ids = pd.read_csv(path_to_tested_ids).squeeze().tolist()
    tested_ids.extend(new_tested_ids)
    pd.Series(tested_ids).to_csv(path_to_tested_ids, header=False, index=False)


if __name__ == "__main__":
    main()

# unwanted_anxiety_flairs = ["Progress!", "Medication", "Therapy", "Share Your Victories"]
