import time

import pandas as pd
import psaw
import requests

url = "https://api.pushshift.io/reddit/search/submission"


def crawl_page(subreddit: str, last_page=None):
    """Crawl a page of results from a given subreddit.

    :param subreddit: The subreddit to crawl.
    :param last_page: The last downloaded page.

    :return: A page or results.
    """
    params = {"subreddit": subreddit, "size": 500, "sort": "desc", "sort_type": "created_utc"}
    if last_page is not None:
        if len(last_page) > 0:
            # resume from where we left at the last page
            params["before"] = last_page[-1]["created_utc"]
        else:
            # the last page was empty, we are past the last page
            return []
    results = requests.get(url, params)
    if not results.ok:
        # something wrong happened
        raise Exception("Server returned status code {}".format(results.status_code))
    return results.json()["data"]


def crawl_subreddit(subreddit, max_submissions=2000):
    """
    Crawl submissions from a subreddit.

    :param subreddit: The subreddit to crawl.
    :param max_submissions: The maximum number of submissions to download.

    :return: A list of submissions.
    """
    submissions = []
    last_page = None
    while last_page != [] and len(submissions) < max_submissions:
        last_page = crawl_page(subreddit, last_page)
        submissions += last_page
        time.sleep(1)
    return submissions[:max_submissions]


def control_group_method(number_of_ids, number_of_subreddits, subreddit_list, output_file=None):  # TODO

    pass


def method2(subreddit, number_of_ids, output_file):
    submissions = crawl_subreddit(subreddit, number_of_ids)

    ids = []

    for submission in submissions:
        ids.append(submission["id"])

    pd.Series(ids).to_csv(output_file, header=False, index=False)


def method1(output_csv, subreddit='all'):
    api = psaw.PushshiftAPI()

    testing = api.search_submissions(subreddit=subreddit, limit=50000)

    post_ids = []

    for sub in testing:
        post_ids.append(sub.id)

    pd.Series(post_ids).to_csv(output_csv, header=False, index=False)


if __name__ == "__main__":
    subreddit_list = pd.read_csv("subreddit_listv2.csv")["subreddit"].tolist()
    control_group_method(100, 1, subreddit_list)
