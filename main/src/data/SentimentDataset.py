import json

from nltk.sentiment.vader import SentimentIntensityAnalyzer

from src.utils.utils import load_json


def add_sentiment_score(dataset):
    json_file = load_json(dataset)
    sentiment_analyzer = SentimentIntensityAnalyzer()

    for sub in json_file:
        sentence = sub["selftext"]
        vlader_output = sentiment_analyzer.polarity_scores(sentence)
        compound_score = vlader_output.get("compound")
        sub["sentiment_score"] = compound_score

    with open(dataset, 'w', encoding='utf-8') as f:
        json.dump(json_file, f, ensure_ascii=False, indent=4)


def main():
    add_sentiment_score("../../data/processed/anxiety_dataset_clean.json")
    add_sentiment_score("../../data/processed/depression_dataset_clean.json")


if __name__ == "__main__":
    main()
