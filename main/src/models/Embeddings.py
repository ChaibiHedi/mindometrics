import logging

import gensim
import numpy as np
from gensim.models.word2vec import Word2Vec
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.svm import LinearSVC

from src.utils.utils import load_data, clean_text_for_analysis


def word_averaging(word_vector, words):
    all_words = set()
    mean = []

    for word in words:

        if isinstance(word, np.ndarray):
            mean.append(word)

        elif word in word_vector.vocab:

            mean.append(word_vector.syn0norm[word_vector.vocab[word].index])
            all_words.add(word_vector.vocab[word].index)

    if not mean:
        logging.warning("cannot compute similarity with no input %s", words)
        return np.zeros(word_vector.vector_size, )

    mean = gensim.matutils.unitvec(np.array(mean).mean(axis=0)).astype(np.float64)

    return mean


def embed(word_vector, words):
    return [word_vector.syn0norm[word_vector.vocab[word].index] if word in word_vector.vocab else [0] * 300 for word in
            words]


def word_averaging_list(word_vector, text_list):
    return np.vstack([word_averaging(word_vector, post) for post in text_list])


def word2vec_svc_model(grid_search=False, distinguish=True):
    wv = gensim.models.KeyedVectors.load_word2vec_format(
        "C:\\Users\\Hedi.Chaibi\\GoogleNews-vectors-negative300.bin.gz",
        binary=True)

    wv.init_sims(replace=True)

    X, y = load_data(pre_processing=False, distinguish=distinguish)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    train_tokenized = [clean_text_for_analysis(x, tokenized=True) for x in X_train]
    test_tokenized = [clean_text_for_analysis(x, tokenized=True) for x in X_test]

    X_train_word_average = word_averaging_list(wv, train_tokenized)

    X_test_word_average = word_averaging_list(wv, test_tokenized)

    if grid_search:

        svm_grid = {'C': [0.1, 0.2, 0.5, 0.75, 1]}
        svm = GridSearchCV(LinearSVC(), svm_grid, refit=True, verbose=3)

    else:

        svm = LinearSVC(C=0.2)

    svm.fit(X_train_word_average, y_train)

    if grid_search:
        print(svm.best_params_)
        print(svm.best_estimator_)

    y_pred = svm.predict(X_test_word_average)

    print(classification_report(y_test, y_pred))
    print(confusion_matrix(y_test, y_pred))


if __name__ == '__main__':
    # word2vec_svc_model()
    # word2vec_svc_model(distinguish=False)

    wv = gensim.models.KeyedVectors.load_word2vec_format(
        "C:\\Users\\Hedi.Chaibi\\GoogleNews-vectors-negative300.bin.gz",
        binary=True)

    wv.init_sims(replace=True)

    print(wv.similarity("anxiety", "depression"))
