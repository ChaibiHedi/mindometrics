import pickle

import gensim
import numpy as np
from gensim.models.word2vec import Word2Vec
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from tensorflow.keras.layers import Dense, LSTM, Embedding, Dropout, Bidirectional
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.sequence import pad_sequences

from src.utils.utils import load_data


def create_embeddings():
    wv = gensim.models.KeyedVectors.load_word2vec_format("/Users/hedichaibi/GoogleNews-vectors-negative300.bin.gz",
                                                         binary=True)

    wv.init_sims(replace=True)

    X__, y = load_data(pre_processing=False, tokenized=True)

    vocab = set(word for sent in X__ for word in sent)

    vocab.add('unk')

    useful_embeddings = {word: wv.vectors_norm[wv.vocab[word].index] for word in vocab if word in wv.vocab}

    vocab_list = []
    embeddings_list = []
    vocab2idx = {}

    for k, v in useful_embeddings.items():
        vocab_list.append(k)
        embeddings_list.append(v)
        vocab2idx[k] = len(vocab_list)

    embeddings_array = np.array([[0] * 300] + embeddings_list)

    X_ = [[vocab2idx.get(word, vocab2idx['unk']) for word in sen] for sen in X__]

    ohe = OneHotEncoder(sparse=False)
    y_ = ohe.fit_transform(y.reshape(-1, 1))

    X = pad_sequences(X_, dtype='float32', padding="post")

    import pickle

    with open('../../data/embeddings/embeddings.pkl', 'wb') as f:
        pickle.dump([vocab_list, embeddings_list, vocab2idx], f)
    with open('../../data/embeddings/x.pkl', 'wb') as f:
        pickle.dump([X, y], f)


def LSTM_Model(grid_search=False):
    with open('../../data/embeddings/embeddings.pkl', 'rb') as f:
        vocab_list, embeddings_list, vocab2idx = pickle.load(f)
        embeddings_array = np.array([[0] * 300] + embeddings_list)

    with open('../../data/embeddings/x.pkl', 'rb') as f:
        X, y = pickle.load(f)

    ohe = OneHotEncoder(sparse=False)
    y_ = ohe.fit_transform(y.reshape(-1, 1))
    X_train, X_test, y_train, y_test = train_test_split(X, y_, test_size=0.2, random_state=42)

    def create_model(lstm_dim, dropout, bidirectional, learning_rate):

        model = Sequential()
        model.add(
            Embedding(input_dim=embeddings_array.shape[0], output_dim=embeddings_array.shape[1],
                      input_length=X.shape[1],
                      weights=[embeddings_array], mask_zero=True))
        if dropout:
            model.add(Dropout(dropout))
        lstm = LSTM(lstm_dim)
        if bidirectional:
            lstm = Bidirectional(lstm)
        model.add(lstm)
        model.add(Dense(3, activation='softmax'))
        opt = Adam(learning_rate=learning_rate)
        model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
        return model

    if grid_search:

        c = KerasClassifier(build_fn=create_model, verbose=100)

        best_model = GridSearchCV(c, dict(
            epochs=[1, 2],
            batch_size=[None],  # , 16,32,64],
            learning_rate=[0.01, 0.001],
            bidirectional=[False],
            dropout=[0, 0.2],  # , 0.5],
            lstm_dim=[50, 100],
        ), verbose=100, n_jobs=-1)

        best_model.fit(X_train, y_train)

        print(best_model.best_estimator_.get_params())

    else:

        model = create_model(50, 0.2, False, 0.001)

        history = model.fit(X_train, y_train, use_multiprocessing=True)


if __name__ == '__main__':
    LSTM_Model()
