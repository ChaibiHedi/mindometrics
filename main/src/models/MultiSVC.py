import copy

import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.svm import LinearSVC

from src.utils.utils import pickle_dump, pickle_load


def create_classifiers_vectorisers():
    X__, y__ = pickle_load("../../data/processed/XandY.pkl")

    X_ = pd.DataFrame(X__)
    y_ = pd.Series(y__)

    dataset = pd.concat([X_, y_], axis=1)
    dataset.columns = ["selftext", "label"]

    train, test = train_test_split(dataset, random_state=42, test_size=0.2)

    train_unhealthy = train[train["label"] != 0]

    train_unhealthy_X = train_unhealthy.values[:, 0]
    train_unhealthy_y = train_unhealthy.values[:, 1]
    train_unhealthy_y = train_unhealthy_y.astype("int")

    vectoriser = TfidfVectorizer(ngram_range=(1, 2), max_features=30000)

    X_transformed = vectoriser.fit_transform(train_unhealthy_X, train_unhealthy_y).toarray()

    pickle_dump(vectoriser, "../../models/Vectoriser-Distinguisher.pkl")

    unhealthy_SVC = LinearSVC(C=0.2)

    unhealthy_SVC.fit(X_transformed, train_unhealthy_y)

    pickle_dump(unhealthy_SVC, "../../models/SVM-Distinguisher.pkl")

    train_binary = copy.copy(train)
    train_binary.loc[train["label"] != 0, "label"] = 1

    train_binary_X = train_binary.values[:, 0]
    train_binary_y = train_binary.values[:, 1].astype("int")

    vectoriser_binary = TfidfVectorizer(ngram_range=(1, 2), max_features=30000)

    X_transformed_binary = vectoriser_binary.fit_transform(train_binary_X, train_binary_y).toarray()

    pickle_dump(vectoriser_binary, "../../models/Vectoriser-Binary.pkl")

    binary_SVC = LinearSVC(C=0.2)

    binary_SVC.fit(X_transformed_binary, train_binary_y)

    pickle_dump(binary_SVC, "../../models/SVM-Binary.pkl")


def hierarchical_model():
    binary_vectoriser = pickle_load("../../models/Vectoriser-Binary.pkl")
    binary_svc = pickle_load("../../models/SVM-Binary.pkl")

    distinguisher_vectoriser = pickle_load("../../models/Vectoriser-Distinguisher.pkl")
    distinguisher_classifier = pickle_load("../../models/SVM-Distinguisher.pkl")

    X, y = pickle_load("../../data/processed/XandY.pkl")

    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=42, test_size=0.2)

    predictions = []

    for row in X_test:

        transformed_row = binary_vectoriser.transform([row])

        output = binary_svc.predict(transformed_row)[0]

        if output == 0:

            predictions.append(0)

        else:

            transformed_row2 = distinguisher_vectoriser.transform([row])

            output2 = distinguisher_classifier.predict(transformed_row2)

            predictions.append(output2[0])

    print(confusion_matrix(y_test, predictions))
    print(classification_report(y_test, predictions))


if __name__ == '__main__':
    hierarchical_model()
