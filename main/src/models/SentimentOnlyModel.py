from collections import Counter

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import train_test_split

from src.utils.utils import load_data_with_sentiment, plot_confusion_matrix


def sentiment_only(distinguish=True):
    X, y = load_data_with_sentiment(pre_processing=False, distinguish=distinguish)
    print(len(X))
    X_train, X_test, y_train, y_test = train_test_split(X[:, 1], y, test_size=0.2, random_state=42)
    print(len(X_train))
    print(Counter(y_train))
    X_train = X_train.reshape(-1, 1)
    X_test = X_test.reshape(-1, 1)
    logreg = LogisticRegression()
    logreg.fit(X_train, y_train)
    y_pred = logreg.predict(X_test)

    cm = confusion_matrix(y_test, y_pred)

    plot_confusion_matrix(cm, ["Healthy", "Unhealthy"], normalize=True)

    print(classification_report(y_test, y_pred))


if __name__ == '__main__':
    # sentiment_only()
    sentiment_only(distinguish=0)
