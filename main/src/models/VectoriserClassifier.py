from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.svm import LinearSVC

from src.utils.utils import load_data, pickle_dump, pickle_load


class VectoriserClassifier:

    def __init__(self, vectoriser, classifier):

        self.vectoriser = vectoriser
        self.classifier = classifier
        self.trained = False

    def fit(self, X_train, y_train):

        transformed_train = self.vectoriser.fit_transform(X_train).toarray()
        self.classifier.fit(transformed_train, y_train)

        self.trained = True

    def predict(self, X_test):

        if self.trained:

            transformer_test = self.vectoriser.transform(X_test).toarray()
            y_pred = self.classifier.predict(transformer_test)
            return y_pred

        else:

            print("Classifier needs to be trained first!")
            return None

    def predict_proba(self, X_test):

        if self.trained:
            transformer_test = self.vectoriser.transform(X_test).toarray()
            y_pred_proba = self.classifier.predict_proba(transformer_test)

            return y_pred_proba

    def test(self, X_test, y_test):

        if self.trained:
            y_pred = self.predict(X_test)

            print(classification_report(y_test, y_pred))
            print(confusion_matrix(y_test, y_pred))

    def save(self, filepath):

        pickle_dump(self, filepath)


def vectoriser_classifier(vectoriser, classifier, distinguish=True):
    # X, y = load_data(pre_processing=True, distinguish=False)

    if distinguish:

        X, y = pickle_load("../../data/processed/XandY.pkl")

    else:

        X, y = pickle_load("../../data/processed/XandYnondistinguished.pkl")

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    vec_classifier = VectoriserClassifier(vectoriser, classifier)

    vec_classifier.fit(X_train, y_train)
    vec_classifier.test(X_test, y_test)


def Tfidf_SVM_Grid_Search():
    vectoriser = TfidfVectorizer(ngram_range=(1, 2), max_features=30000)

    X, y = load_data(pre_processing=True, sentiment=False)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    transformed_X_train = vectoriser.fit_transform(X_train).toarray()

    svm_grid = {'C': [0.1, 0.2, 0.5, 0.75, 1, 5, 10, 100, 1000]}

    grid = GridSearchCV(LinearSVC(), svm_grid, refit=True, verbose=3)

    grid.fit(transformed_X_train, y_train)

    print(grid.best_params_)
    print(grid.best_estimator_)

    transformed_X_test = vectoriser.transform(X_test).toarray()

    y_pred = grid.predict(transformed_X_test)

    print(classification_report(y_test, y_pred))


if __name__ == "__main__":
    # svm = LinearSVC()
    # count_vectoriser = CountVectorizer(ngram_range=(1, 1), max_features=30000)
    # vectoriser_classifier(count_vectoriser, svm, distinguish=False)
    #
    # svm2 = LinearSVC()
    # count_vectoriser2 = CountVectorizer(ngram_range=(1, 2), max_features=30000)
    # vectoriser_classifier(count_vectoriser2, svm2, distinguish=False)
    #
    # svm3 = LinearSVC()
    # count_vectoriser3 = CountVectorizer(ngram_range=(1, 1), max_features=30000)
    # vectoriser_classifier(count_vectoriser3, svm3, distinguish=True)
    #
    # svm4 = LinearSVC()
    # count_vectoriser4 = CountVectorizer(ngram_range=(1, 2), max_features=30000)
    # vectoriser_classifier(count_vectoriser4, svm4, distinguish=True)

    # svm = LinearSVC()
    # tfidf_vectoriser = TfidfVectorizer(ngram_range=(1, 1), max_features=30000)
    # vectoriser_classifier(tfidf_vectoriser, svm, False)
    #
    # svm2 = LinearSVC()
    # tfidf_vectoriser2 = TfidfVectorizer(ngram_range=(1, 2), max_features=30000)
    # vectoriser_classifier(tfidf_vectoriser2, svm2, False)
    #
    # svm3 = LinearSVC()
    # tfidf_vectoriser3 = TfidfVectorizer(ngram_range=(1, 1), max_features=30000)
    # vectoriser_classifier(tfidf_vectoriser3, svm3, True)
    #
    # svm4 = LinearSVC()
    # tfidf_vectoriser4 = TfidfVectorizer(ngram_range=(1, 2), max_features=30000)
    # vectoriser_classifier(tfidf_vectoriser4, svm4, True)

    svm4 = LinearSVC(C=0.2)
    tfidf_vectoriser4 = TfidfVectorizer(ngram_range=(1, 2), max_features=30000)
    vectoriser_classifier(tfidf_vectoriser4, svm4, True)

    svm5 = LinearSVC(C=0.2)
    tfidf_vectoriser5 = TfidfVectorizer(ngram_range=(1, 2), max_features=30000)
    vectoriser_classifier(tfidf_vectoriser5, svm5, False)
