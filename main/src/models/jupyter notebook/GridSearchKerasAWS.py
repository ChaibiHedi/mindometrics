import pickle

import numpy as np
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.preprocessing import OneHotEncoder
from tensorflow.keras.layers import Dense, LSTM, Embedding, Dropout, Bidirectional
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam

with open("embeddings.pkl", "rb") as f:
    vocab_list, embeddings_list, vocab2idx = pickle.load(f)
    embeddings_array = np.array([[0] * 300] + embeddings_list)

with open("x.pkl", "rb") as f:
    X, y = pickle.load(f)

ohe = OneHotEncoder(sparse=False)
y_ = ohe.fit_transform(y.reshape(-1, 1))
X_train, X_test, y_train, y_test = train_test_split(X, y_, test_size=0.2, random_state=42)


def create_model(lstm_dim, dropout, bidirectional, learning_rate):
    model = Sequential()
    model.add(
        Embedding(input_dim=embeddings_array.shape[0], output_dim=embeddings_array.shape[1], input_length=X.shape[1],
                  weights=[embeddings_array], mask_zero=True))
    if dropout:
        model.add(Dropout(dropout))
    lstm = LSTM(lstm_dim)
    if bidirectional:
        lstm = Bidirectional(lstm)
    model.add(lstm)
    model.add(Dense(3, activation="softmax"))
    opt = Adam(learning_rate=learning_rate)
    model.compile(loss="categorical_crossentropy", optimizer=opt, metrics=["accuracy"])
    return model


c = KerasClassifier(build_fn=create_model, verbose=100)

best_model = GridSearchCV(c, dict(
    epochs=[1, 2],
    batch_size=[16, 32, 64],
    learning_rate=[0.01, 0.001, 0.0001],
    bidirectional=[False, True],
    dropout=[0, 0.2, 0.5],
    lstm_dim=[50, 75, 100]
), verbose=100, n_jobs=-1)

best_model.fit(X_train, y_train)

with open("best_model.pkl", "wb") as f:
    pickle.dump(best_model, f)
