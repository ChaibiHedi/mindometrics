import itertools
import json
import pickle as pkl
import string

import contractions
import matplotlib.pyplot as plt
import nltk
import numpy as np
import pandas as pd
from nltk import WordNetLemmatizer
from nltk.corpus import wordnet
# The plot_confusin_matrix method is taken from the scikit-learn package documentation.
# Available at: https://scikit-learn.org/0.18/auto_examples/model_selection/plot_confusion_matrix.html
from sklearn.metrics import classification_report, confusion_matrix


def plot_confusion_matrix(cm, classes,
                          normalize=True,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues,
                          save=None):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """

    np.set_printoptions(precision=2)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, "{:0.2f}".format(cm[i, j]),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

    if save is not None:
        plt.gcf().subplots_adjust(bottom=0.20)
        plt.savefig(save, dpi=300)

    plt.show()


if __name__ == '__main__':
    y_true = ["Healthy"] * 50 + ["Depression"] * 50
    y_pred = ["Healthy"] * 43 + ["Depression"] * 42 + ["Healthy"] * 15

    print(confusion_matrix(y_true, y_pred))
    print(classification_report(y_true, y_pred))


def get_wordnet_pos(tag):
    if tag.startswith('J'):
        return wordnet.ADJ
    elif tag.startswith('V'):
        return wordnet.VERB
    elif tag.startswith('N'):
        return wordnet.NOUN
    elif tag.startswith('R'):
        return wordnet.ADV
    else:
        return wordnet.NOUN


def clean_text_for_analysis(text: str, stopwords=True, tokenized=False):
    split_text = text.split()

    no_contractions = [contractions.fix(word) for word in split_text]

    rejoined = " ".join(no_contractions)

    tokens_list = nltk.tokenize.word_tokenize(rejoined)

    lower_case = [word.lower() for word in tokens_list]

    punc = string.punctuation

    no_punct = [token for token in lower_case if token not in punc and token.isalnum()]

    if not stopwords:

        return no_punct

    else:

        stop_words = set(nltk.corpus.stopwords.words('english'))

        text_no_stopwords = [token for token in no_punct if token not in stop_words]

        pos_tags = nltk.tag.pos_tag(text_no_stopwords)

        wordnet_tags = [(word, get_wordnet_pos(tag)) for (word, tag) in pos_tags]

        word_lemmatiser = WordNetLemmatizer()

        lemmatised = [word_lemmatiser.lemmatize(word, pos) for (word, pos) in wordnet_tags]

        if tokenized:
            return lemmatised
        else:
            return " ".join(lemmatised)


def load_json(dataset):
    with open(dataset) as f:
        json_file = json.load(f)
    return json_file


def dump_json(dataset, json_file):
    with open(dataset, 'w', encoding='utf-8') as f:
        json.dump(json_file, f, ensure_ascii=False, indent=4)


def load_data_with_sentiment(pre_processing=False, distinguish=True):
    # TODO: change load_data_with_sentiment to version of load_data by just adding the sentiment column

    anxiety = pd.read_json("../../data/processed/anxiety_dataset_clean.json")
    depression = pd.read_json("../../data/processed/depression_dataset_clean.json")
    control = pd.read_json("../../data/processed/control_dataset_clean.json")

    if pre_processing:

        X = pd.concat([anxiety["selftext"],
                       depression["selftext"],
                       control["selftext"]]).map(clean_text_for_analysis).values

    else:

        X = pd.concat([anxiety[["selftext", "sentiment_score"]],
                       depression[["selftext", "sentiment_score"]],
                       control[["selftext", "sentiment_score"]]]).values

    if distinguish:

        y = np.array([1] * len(anxiety) + [2] * len(depression) + [0] * len(control))

    else:

        y = np.array([1] * (len(anxiety) + len(depression)) + [0] * len(control))

    return X, y


def load_data(pre_processing=False, distinguish=True, tokenized=False):
    anxiety = pd.read_json("../../data/processed/anxiety_dataset_clean.json")
    depression = pd.read_json("../../data/processed/depression_dataset_clean.json")
    control = pd.read_json("../../data/processed/control_dataset_clean.json")

    if pre_processing:

        X = pd.concat([anxiety["selftext"],
                       depression["selftext"],
                       control["selftext"]]).map(lambda x: clean_text_for_analysis(x, tokenized)).values

    else:

        X = pd.concat([anxiety["selftext"],
                       depression["selftext"],
                       control["selftext"]]).values

    if distinguish:

        y = np.array([1] * len(anxiety) + [2] * len(depression) + [0] * len(control))

    else:

        y = np.array([1] * (len(anxiety) + len(depression)) + [0] * len(control))

    return X, y


def load_anxiety_depression_data():
    anxiety = pd.read_json("../../data/processed/anxiety_dataset_clean.json")
    depression = pd.read_json("../../data/processed/depression_dataset_clean.json")

    X = pd.concat([anxiety["selftext"],
                   depression["selftext"]]).map(clean_text_for_analysis).values

    y = np.array([1] * len(anxiety) + [2] * len(depression))

    return X, y


def pickle_load(filename):
    with open(filename, "rb") as input_file:
        pkl_object = pkl.load(input_file)

    return pkl_object


def pickle_dump(object, filename):
    with open(filename, "wb") as output_file:
        pkl.dump(object, output_file)


def remove_duplicates_from_json(json_file, index):
    with open(json_file) as f:
        ds = json.load(f)

    new_json_file = []
    ids_in_file = []

    for sub in ds:

        if sub[index] not in ids_in_file:
            new_json_file.append(sub)
            ids_in_file.append(sub[index])

    with open(json_file, "w", encoding="utf-8") as f:
        json.dump(new_json_file, f, ensure_ascii=False, indent=4)

    print(f"There were {len(ds) - len(new_json_file)} duplicates removed.")


def add_instances_to_json_file(instance_list, filename):
    with open(filename) as f:
        json_file = json.load(f)

    for instance in instance_list:
        json_file.append(instance)

    with open(filename, 'w', encoding='utf-8') as f:
        json.dump(json_file, f, ensure_ascii=False, indent=4)


def get_field_list_from_json(field, filename):
    query_result = []

    with open(filename) as f:
        json_file = json.load(f)

    for instance in json_file:
        query_result.append(instance[field])

    return query_result
