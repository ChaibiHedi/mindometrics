from collections import Counter

import flair
import matplotlib
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from nltk.sentiment.vader import SentimentIntensityAnalyzer

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


def compare_corpus_lengths(dataset1, dataset2, dataset3):
    df1 = pd.read_json(dataset1)
    df2 = pd.read_json(dataset2)
    df3 = pd.read_json(dataset3)

    df1["length"] = df1["selftext"].apply(len)
    df2["length"] = df2["selftext"].apply(len)

    plt.figure(figsize=(1920 / 96, 1080 / 96), dpi=96)

    plt.boxplot([df1["length"], df2["length"], df3["length"]], labels=["Anxiety", "Depression", "Control"])

    plt.savefig("BoxPlotLengthText.png", dpi=96)

    plt.show()


def compare_sentiment_distribution(dataset1, dataset2, dataset3, method):
    df1 = pd.read_csv(dataset1)["vader_score"]
    df2 = pd.read_csv(dataset2)["vader_score"]
    df3 = pd.read_csv(dataset3)["vader_score"]

    if method == "histogram":

        bins = np.linspace(-1, 1, 60)

        with matplotlib.style.context("dark_background"):
            plt.figure(figsize=(1920 / 96, 1080 / 96), dpi=96)

            plt.hist([df1, df2, df3], bins, alpha=0.5,
                     label=["Anxiety", "Depression", "Control"])

            plt.legend(loc='upper right')
            plt.title("Distribution of Sentiment Polarity for Anxiety vs. Depression vs. Control")

            plt.savefig("Histogram_Sentiment_AnxietyDepressionControl.png", dpi=96)
            plt.show()

    elif method == "boxplot":

        plt.figure(figsize=(1920 / 96, 1080 / 96), dpi=96)

        plt.boxplot([df1, df2, df3], labels=["Anxiety", "Depression", "Control"])

        plt.savefig("BoxPlotSentimentPolarity.png", dpi=96)

        plt.show()

    else:

        return


def sentiment_analysis(dataset, output_file=None):
    dataset = pd.read_json(dataset)

    dataset_corpus = dataset["selftext"]

    anxiety_sid = SentimentIntensityAnalyzer()
    flair_sentiment = flair.models.TextClassifier.load("en-sentiment")

    vader_output_list = []
    flair_output_list = []
    flair_score_list = []

    for i in range(len(dataset_corpus)):

        if i % 1000 == 0:
            print(f"Doing iteration {i}/{len(dataset_corpus)}.")

        sentence = dataset_corpus[i]

        s = flair.data.Sentence(sentence)
        vlader_output = anxiety_sid.polarity_scores(sentence)
        flair_sentiment.predict(s)

        flair_output_list.append(s.labels[0]._value)
        flair_score_list.append(s.labels[0]._score)

        compound_score = vlader_output.get("compound")

        vader_output_list.append(compound_score)

    dataset["vader_score"] = vader_output_list
    dataset["flair_label"] = flair_output_list
    dataset["flair_score"] = flair_score_list

    dataset["vader_label"] = dataset["vader_score"].apply(lambda x: transform_vader(x))

    if output_file is not None:
        dataset.to_csv(output_file, index=False)

    plt.show()


def transform_vader(value):
    if value < -0.05:
        return "NEGATIVE"

    elif value < 0.05:
        return "NEUTRAL"

    else:
        return "POSITIVE"


def analyse_sentiment_results(dataset):
    dataset = pd.read_csv(dataset)

    sentiment_dataset = dataset[["vader_label", "vader_score", "flair_label", "flair_score"]]

    vader_label_counter = Counter(sentiment_dataset["vader_label"])

    negative_vader_count = vader_label_counter["NEGATIVE"]
    neutral_vader_count = vader_label_counter.get("NEUTRAL")
    positive_vader_count = vader_label_counter.get("POSITIVE")

    print(f"Using the VADER model,"
          f" there are {round(100 * negative_vader_count / len(dataset))}% of negative instances, "
          f"{round(100 * neutral_vader_count / len(dataset))}% of neutral instances "
          f"and {round(100 * positive_vader_count / len(dataset))}% of positive instances.")

    flair_label_counter = Counter(sentiment_dataset["flair_label"])

    positive_flair_count = flair_label_counter.get("POSITIVE")
    negative_flair_count = flair_label_counter.get("NEGATIVE")

    print(f"Using the FLAIR model,"
          f" there are {round(100 * negative_flair_count / len(dataset))}% of negative instances and "
          f"{round(100 * positive_flair_count / len(dataset))}% of positive instances.")

    sentiment_dataset_no_neutral = sentiment_dataset[sentiment_dataset["vader_label"] != "NEUTRAL"]

    label_match = np.where(sentiment_dataset_no_neutral["flair_label"] == sentiment_dataset_no_neutral["vader_label"],
                           True, False)

    print(sum(label_match) / len(sentiment_dataset_no_neutral))

    print("\n")

    sentiment_dataset["vader_score"].plot.hist()

    plt.show()


def main():
    sentiment_analysis("../../data/processed/anxiety_dataset_clean.json", "anxiety_sentiment_analysis_result.csv")
    sentiment_analysis("../../data/processed/depression_dataset_clean.json", "depression_sentiment_analysis_result"
                                                                             ".csv")
    sentiment_analysis("../../data/processed/control_dataset_clean.json", "depression_sentiment_analysis_result"
                                                                          ".csv")


if __name__ == "__main__":
    main()
