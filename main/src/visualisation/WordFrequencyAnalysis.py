import random

import cufflinks as cf
import matplotlib
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib_venn import venn2, venn2_circles, venn3, venn3_circles
from nltk import FreqDist
from sklearn.feature_extraction.text import CountVectorizer
from wordcloud import WordCloud

from src.utils.utils import clean_text_for_analysis

cf.go_offline()
cf.set_config_file(offline=False, world_readable=True)
pd.set_option('display.max_rows', 500)
pd.set_option("display.max_colwidth", 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 2000)


def get_top_k_ngram(corpus, n, k):
    vec = CountVectorizer(ngram_range=(n, n), stop_words='english').fit(corpus)
    bag_of_words = vec.transform(corpus)
    sum_words = bag_of_words.sum(axis=0)
    words_freq = [(word, sum_words[0, idx]) for word, idx in vec.vocabulary_.items()]
    words_freq = sorted(words_freq, key=lambda x: x[1], reverse=True)

    return words_freq[:k]


def ngram_analysis(dataset: str, n=2, plot_file=None):
    if n == 2:
        prefix = "Bi"

    elif n == 3:
        prefix = "Tri"

    else:
        prefix = str(n)

    df = pd.read_json(dataset)

    df["clean_selftext"] = df["selftext"].apply(clean_text_for_analysis)

    ngrams = get_top_k_ngram(df['clean_selftext'], n, 30)

    with matplotlib.style.context("ggplot"):

        df_plot = pd.DataFrame(ngrams, columns=['selftext', 'count'])

        plt.gcf().subplots_adjust(bottom=0.35)

        df_plot.groupby('selftext').sum()['count'].sort_values(ascending=False).plot.bar()

        plt.title(f"{prefix}-gram Frequency Analysis", y=1.08)
        plt.xlabel(f"{prefix}-grams")
        plt.ylabel("Corpus Frequency")

        if plot_file is not None:
            plt.savefig(plot_file, dpi=300)

        plt.show()


def generate_wordcloud(dataset, save_fig=None):
    frequent_words = n_most_frequent_words(dataset)

    wordcloud = WordCloud(width=1920, height=1080, background_color='white').generate(str(frequent_words))
    fig = plt.figure(figsize=(30, 10), facecolor='white')
    plt.imshow(wordcloud)
    plt.axis('off')
    plt.title('Top 100 Most Common Words', fontsize=50)
    plt.tight_layout(pad=0)
    plt.show()

    if save_fig is not None:
        wordcloud.to_file(save_fig)


def n_most_frequent_words(dataset, n=100):
    dataset = pd.read_json(dataset)
    dataset["clean_selftext"] = dataset["selftext"].apply(clean_text_for_analysis)
    words_list = []

    for i in range(len(dataset)):
        sentence = dataset["clean_selftext"].iloc[i]
        words = sentence.split(" ")
        words_list.extend(words)

    frequent_words = FreqDist(words_list).most_common(n)
    return frequent_words


def generate_venn(dataset1, dataset2, dataset3=None):
    frequent_words1 = n_most_frequent_words(dataset1, 100)
    frequent_words2 = n_most_frequent_words(dataset2, 100)

    words_set1 = []
    words_set2 = []

    sets = []

    for word in frequent_words1:
        words_set1.append(word[0])

    for word in frequent_words2:
        words_set2.append(word[0])

    words_set1 = set(words_set1)
    words_set2 = set(words_set2)

    sets.append(words_set1)
    sets.append(words_set2)

    if dataset3 is not None:

        frequent_words3 = n_most_frequent_words(dataset3, 100)

        words_set3 = []

        for word in frequent_words3:
            words_set3.append(word[0])

        words_set3 = set(words_set3)

        sets.append(words_set3)

    # plt.figure(figsize=(30, 10), dpi=96)
    #
    # plt.gcf().subplots_adjust(bottom=0.25)

    if dataset3 is None:

        v = venn2(sets,
                  set_labels=["Anxiety", "Depression"])

    else:

        v = venn3(sets, set_labels=["Anxiety", "Depression", "Control"])

    set1_only = words_set1 - words_set2
    set2_only = words_set2 - words_set1

    overlap = random.sample(words_set1.intersection(words_set2), k=25)

    text_left = ((40 * " " + "\n").join(set1_only)) + (40 * " ")
    text_right = 40 * " " + (("\n" + 40 * " ").join(list(set2_only)[:-1])) + "\n" + (40 * " ") + list(set2_only)[-1]

    v.get_label_by_id("10").set_text(text_left)
    v.get_label_by_id("01").set_text(text_right)
    v.get_label_by_id("11").set_text("\n".join(overlap))

    for text in v.set_labels:
        text.set_fontsize(8)
    for text in v.subset_labels:
        text.set_fontsize(8)

    if dataset3 is None:

        c = venn2_circles(sets,
                          linestyle="solid")

    else:

        c = venn3_circles(sets, linestyle="solid")

    plt.savefig("venndiagramcorrected.png", dpi=96)

    plt.show()


if __name__ == "__main__":
    ngram_analysis("../../data/processed/anxiety_dataset_clean.json", n=2, plot_file="anxiety_bigrams.png")
    ngram_analysis("../../data/processed/anxiety_dataset_clean.json", n=3, plot_file="anxiety_trigrams.png")
    ngram_analysis("../../data/processed/depression_dataset_clean.json", n=2, plot_file="depression_bigrams.png")
    ngram_analysis("../../data/processed/depression_dataset_clean.json", n=3, plot_file="depression_trigrams.png")

    # generate_venn("../../data/processed/anxiety_dataset_clean.json",
    #               "../../data/processed/depression_dataset_clean.json")

    # generate_wordcloud("../../data/processed/anxiety_dataset_clean.json", "anxiety_wordcloud.png")
    # generate_wordcloud("../../data/processed/depression_dataset_clean.json", "depression_wordcloud.png")
    # generate_wordcloud("../../data/processed/control_dataset_clean.json", "control_wordcloud.png")
